'use strict';

require('colors');

const _ = require('lodash');
const winston = require('winston');

const defaultLevel = 'http';
const defaultSilent = false;

class Logger {
    constructor (options) {
        this.logger = winston.createLogger(_.defaultsDeep({}, options || {}, {
            level: defaultLevel,
            silent: defaultSilent,
            format: winston.format.combine(
                winston.format.simple(),
                winston.format.timestamp(),
                winston.format.printf(info => `[${info.timestamp}] - ${info.level.toUpperCase()} - ${info.message}`)
            ),
            transports: [new winston.transports.Console()]
        }));
    }

    error (...messages) {
        this.log('error', ['✘ Ooops...'.red].concat(messages));
    }

    warn (...messages) {
        this.log('warn', ['⚠'.yellow].concat(messages));
    }

    info (...messages) {
        this.log('info', messages);
    }

    title (...messages) {
        this.log('info', _.map(['==========  '].concat(messages).concat('  =========='), msg => `${msg}`.magenta));
    }

    success (...messages) {
        this.log('info', ['✔'.green].concat(messages));
    }

    step (...messages) {
        this.log('info', ['•'.blue].concat(messages));
    }

    lap (...messages) {
        this.log('info', ['✦'.magenta].concat(messages));
    }

    arrow (...messages) {
        this.log('info', ['➜'.yellow].concat(messages));
    }

    json (...messages) {
        this.log('info', _.map(Array.from(arguments), msg => JSON.stringify(msg, null, 2)));
    }

    log (method, messages) {
        this.logger[method].apply(this.logger, [Array.from(messages).join(' ')]); // eslint-disable-line no-useless-call
    }

    write (message, encoding) {
        this.log('http', [_.trim(message)]);
    }
}

module.exports = Logger;
