'use strict';

const chai = require('chai');

global.sinon = require('mocha-sinon');
global.chai = chai;

global.assert = chai.assert;
global.expect = chai.expect;
global.should = chai.should();
